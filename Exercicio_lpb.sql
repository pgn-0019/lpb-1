-- Implementacao do banco de dados trabalho
create database if exists trabalho;

create table cliente (
  codigo int,
  cpf char(11),
  nome varchar(30),
  endereco varchar(30),
  constraint "cliente_codigo_pk" primary key (codigo));

create table projeto (
  codigo int,
  descricao varchar(60),
  constraint "projeto_codido_pk" primary key (codigo));

create table colaborador (
  codigo int,
  cpf char(11),
  nome varchar(30),
  depto char(3) constraint "colaborador_depto_check" check (depto in ('TI','RH','SAC')),
  endereco varchar(30),
  constraint "colaborador_codigo_pk" primary key (codigo));

create table trabalho (
  cod_proj int,
  cod_col int,
  pagamento numeric,
  data date,
  constraint "trabalho_proj_col_pk" primary key (cod_proj,cod_col),
  constraint "trabalho_projeto_fk" foreign key (cod_proj) references projeto,
  constraint "trabalho_colaborador_fk" foreign key (cod_col) references colaborador
)  

-- Dados das tabelas

insert into projeto values (1,'Web'),(2,'Veredas');

insert into cliente values (1,'11111111111','Ana','Rua1'),
(2,'22222222222','Rodrigo','Rua2');

insert into colaborador values (1,'11111111111','Ana','TI','Rua1'),
(2,'33333333333','Alan','TI','Rua2'),
(3,'44444444444','Betina','RH','Rua3'),
(4,'55555555555','Luciano','SAC','Rua10');
-- A tabela trabalho fica vazia por enquanto!!

-- Consultas do exercício.
-- a) Imprimir o nome de todos os colaboradores. 

create or replace function imprimir_colaboradores() returns setof varchar as $body$
begin
   return query select nome from colaborador;
   return;
end
$body$ language plpgsql

select * from imprimir_colaboradores();

-- b) Imprimir o nome de todos os clientes que moram em Brasília - endereco. 

create or replace function imprimir_cliente_endereco(endereco varchar) returns setof varchar as $body$
begin
   return query select nome from cliente c where c.endereco=$1;
   return;
end
$body$ language plpgsql

select * from imprimir_cliente_endereco('Brasilia');

-- c1) Imprimir o nome dos colaboradores que tambem são clientes. 

create or replace function imprimir_colaboradores_clientes() returns setof varchar as $body$
begin
   return query select co.nome from colaborador co join cliente c on (co.cpf=c.cpf);
   return;
end
$body$ language plpgsql

select * from imprimir_colaboradores_clientes();

-- c2) Imprimir nome dos colaboradores que também são clientes 
-- Usando intersect 
create or replace function imprimir_colaboradores_clientes() returns setof varchar as $body$
begin
   return query select co.nome from colaborador co intersect select c.nome from cliente c;
   return;
end
$body$ language plpgsql

select * from imprimir_colaboradores_clientes();

-- c3) Imprimir o nome dos colaboradores que tambem são clientes. 
-- usando subconsulta
create or replace function imprimir_colaboradores_clientes() returns setof varchar as $body$
begin
   return query select co.nome from colaborador co where co.cpf in (select cpf from cliente); 
   return;
end
$body$ language plpgsql

select * from imprimir_colaboradores_clientes();

-- d1) Imprimir o nome dos clientes que não são colaboradores. 

create or replace function imprimir_clientes_nao_colaboradores() returns setof varchar as $body$
begin
   return query select c.nome from cliente c where c.cpf not in (select cpf from colaborador); 
   return;
end
$body$ language plpgsql

select * from imprimir_clientes_nao_colaboradores();

-- d2) Imprimir o nome dos clientes que não são colaboradores. 
-- Usando left join 
-- drop function if exists imprimir_clientes_nao_colaboradores();
create or replace function imprimir_clientes_nao_colaboradores() returns setof varchar as $body$
begin
   return query select c.nome from cliente c left join colaborador co 
   on (c.cpf=co.cpf)
   where (co.nome is null);
   return;
end
$body$ language plpgsql

select * from imprimir_clientes_nao_colaboradores();

--d3) Imprimir o nome dos clientes que não são colaboradores.  

create or replace function imprimir_clientes_nao_colaboradores() returns setof varchar as $body$
declare a varchar;
        b varchar;
begin
   for a,b in select c.nome,co.nome from cliente c left join colaborador co 
	on (c.cpf=co.cpf) loop
	if (b is null) then
	  return next a;
	end if;
   end loop;	
   return;
end
$body$ language plpgsql

select * from imprimir_clientes_nao_colaboradores();

-- d4) Imprimir o nome dos clientes que não são colaboradores. 
-- Usando cursor explicito
create or replace function imprimir_clientes_nao_colaboradores() returns setof varchar as $body$
declare clie_col_cursor cursor for select c.nome,co.nome from cliente c left join colaborador co 
	on (c.cpf=co.cpf);
	nome_clie varchar;
	nome_col varchar; 
begin
   open clie_col_cursor;
   loop
	fetch clie_col_cursor into nome_clie, nome_col;
	if (nome_col is null) then
	   if (nome_clie is not null) then	
		return next nome_clie;
	   end if;
	end if;
	exit when not found; -- not found (se o cursor for nulo) 
   end loop;	
   return;
end
$body$ language plpgsql

select * from imprimir_clientes_nao_colaboradores();

--e1)
-- Usando subconsulta na clausula having

create or replace function maior_salario_RH() returns varchar as $body$
declare nome_cursor cursor for 
	select c.nome, sum(t.pagamento) 
	from colaborador c join trabalho t on (c.codigo = t.cod_col)
	join projeto p on (t.cod_proj = p.codigo)
	where c.depto='RH'
	group by c.nome
	having (sum(t.pagamento) > ANY (select sum(t.pagamento) as total
		     from colaborador c join trabalho t on (c.codigo = t.cod_col)
                     join projeto p on (t.cod_proj = p.codigo)
                     where c.depto='SAC'
                     group by c.nome));
        -- duas variaveis locais a mais:
        nomec varchar;
        pagamentoc decimal;             
begin
   open nome_cursor;
   loop
	fetch nome_cursor into nomec,pagamentoc;
	if (nomec is not null) then
	   return nomec;	
	end if;
	exit when not found;
   end loop;                    
end
$body$ language plpgsql    

select maior_salario_RH()                 