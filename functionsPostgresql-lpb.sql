﻿CREATE OR REPLACE FUNCTION SOMA(a integer,b integer) RETURNS integer AS $$
BEGIN
	RETURN a + b;
END;
$$ language plpgsql;

-- Usando a função soma
SELECT SOMA(3,5);

-- Funcao dropa a função caso ela exista
DROP FUNCTION IF EXISTS PRINT(a text);

CREATE OR REPLACE FUNCTION PRINT(a text) RETURNS TEXT AS $$
BEGIN
	RETURN a;
END
$$ language plpgsql;

-- Funcao printa usando a função print
SELECT PRINT('Usando stored function postgres');

CREATE OR REPLACE FUNCTION VERDADEIRO_FALSO(I BIT) RETURNS TEXT AS $$
BEGIN
	-- Uso de type cast explicito do postgresql --> ::bit
	-- Caso contrário o postgres trata o valor como inteiro
	IF (I = 0::BIT) THEN
		RETURN 'FALSO';
	ELSE
		RETURN 'VERDADEIRO';
	END IF;
END
$$ language plpgsql;

-- Uso da funcao verdadeiro ou falso
SELECT VERDADEIRO_FALSO(0::BIT);
SELECT VERDADEIRO_FALSO(1::BIT);



-- plpgsql significa programming language postgressql;


--DROP TABLE IF EXISTS COMPRA;
--DROP TABLE IF EXISTS CLIENTE; 

CREATE TABLE CLIENTE (
  CODIGO INTEGER,
  NOME VARCHAR(60),
  PRIMARY KEY(CODIGO));

-- USANDO STORED FUNCTION PARA PREENCHER A TABELA DE CLIENTES

CREATE OR REPLACE FUNCTION INSERE_CLIENTE(CODIGO INTEGER, NOME VARCHAR) RETURNS VOID AS $$
BEGIN
	-- $1 E $2 REPRESENTA OS PARAMETROS POR POSIÇÃO
	INSERT INTO CLIENTE VALUES($1,$2);
	RETURN;
END
$$ LANGUAGE plpgsql;

-- USO DA FUNÇÃO INSERE_CLIENTE;
SELECT INSERE_CLIENTE(1,'Aline');

-- CONFERINDO SE HOUVE INSERÇÃO
SELECT * FROM CLIENTE;

DROP FUNCTION IF EXISTS INSERE_CLIENTE(CODIGO INTEGER, NOME VARCHAR);

-- MODIFICANDO A FUNCAO PARA CHECAR SE O CLIENTE JÁ EXISTE
CREATE OR REPLACE FUNCTION INSERE_CLIENTE(CODIGO INTEGER, NOME VARCHAR) RETURNS VARCHAR AS $$
DECLARE
   TEMP INT;
BEGIN
	SELECT C.CODIGO FROM CLIENTE C WHERE C.CODIGO = $1 INTO TEMP;
	IF (TEMP IS NOT NULL) THEN
	   RETURN 'JÁ EXISTE UM CLIENTE CADASTRADO COM ESTE CÓDIGO';
	ELSE
           INSERT INTO CLIENTE VALUES($1,$2);
           RETURN 'INSERÇÃO EXECUTADA COM SUCESSO';	       			
	END IF;
END
$$ LANGUAGE PLPGSQL;

--USANDO A FUNÇÃO NOVAMENTE
SELECT INSERE_CLIENTE(1,'André');
SELECT INSERE_CLIENTE(2,'Karine');

-- Outra versão da mesma stored function
-- MODIFICANDO A FUNCAO PARA CHECAR SE O CLIENTE JÁ EXISTE
CREATE OR REPLACE FUNCTION INSERT_CLIENTE(CODIGO INTEGER, NOME VARCHAR) RETURNS VOID AS $$
DECLARE
   TEMP INT;
BEGIN
	SELECT C.CODIGO FROM CLIENTE C WHERE C.CODIGO = $1 INTO TEMP;
	IF (TEMP IS NOT NULL) THEN
	   RAISE NOTICE 'JÁ EXISTE UM CLIENTE CADASTRADO COM O CÓDIGO %', TEMP;
	ELSE
           INSERT INTO CLIENTE VALUES($1,$2);
           RAISE NOTICE 'INSERÇÃO EXECUTADA COM SUCESSO COM CODIGO %', TEMP;	       			
	END IF;
END
$$ LANGUAGE PLPGSQL;

SELECT INSERT_CLIENTE(2,'Vanessa');


