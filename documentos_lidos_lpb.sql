create table documentos_lidos (
 id serial,
 documento text,
 lido bit,
 constraint "documentos_lidos_pk" primary key (id));

insert into documentos_lidos(documento, lido) values ('Administrativo.pdf',1::bit),
('Jurisprudencia do STF.pdf',0::bit); 

create or replace function valida_bit(numero bit) returns varchar(30) as $body$
begin
  if (numero = 1::bit) then
	return 'VERDADEIRO';
  else
	return 'FALSO';
  end if;
end
$body$ language plpgsql  -- Programming Language PostGreSQL

select documento, valida_bit(lido) from documentos_lidos;